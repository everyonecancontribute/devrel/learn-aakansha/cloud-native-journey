# Jenkins

**CI/CD**

Its a way to take code and package it up and deploy it to a system it could be a serverless system or lambda or an Azure function, could be some container running somewhere with Docker.

CI - continuous integration —> you’re taking the code, packaging it up, and then you’re giving it to the CD process.

CD - continuous delivery/deployment —> is the process where you deploy the code to some system.

deployment —> once the code is packaged and tested and all that good stuff in the CI process. you then deliver it to whatever system you running it to.

**Difference bw Continuous deployment and delivery**

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled.png)

No human intervention in continuous deployment.

**Why Jenkins?**

- free
- open-source
- ton of plugin

cat id_rsa.pub —> to see the public ssh key for authentication with Jenkins

**Installation of Jenkins on VM  —>** 

Resources to follow :

[https://learn.microsoft.com/en-us/azure/developer/jenkins/configure-on-linux-vm](https://learn.microsoft.com/en-us/azure/developer/jenkins/configure-on-linux-vm)

[https://www.jenkins.io/doc/book/installing/linux/#debianubuntu](https://www.jenkins.io/doc/book/installing/linux/#debianubuntu)

- run up a VM
- `ssh azureuser@<ip_address>` ( now you into the server )
- sudo apt update -y
- Install java - only prerequisite needed

We have configured the SSH service in Jenkins to listen on a fixed port. To find out the port in use, run the command below:

`curl -Lv http://localhost:8085/login 2>&1 | grep -i 'x-ssh-endpoint'`  

ssh -i jenkins_key -l mike -p 8022 jenkins-server help

**What are Plugins ?**

Plugins allows you to connect to other services, for eg you want to connect to azure from Jenkins, then download the Jenkins plugin.

**Plugins Index**

If a plugin requires restart then do → sudo systemctl restart jenkins

then check status - systemctl status jenkins

To restart jenkins after uninstalling a plugin 
`java -jar jenkins-cli.jar -s http://localhost:8085 -auth 'admin:Adm!n321' restart`

—> here username and pass is admin:Adm!n321
To use Jenkins from CLI 
`java -jar jenkins-cli.jar -s http://localhost:8085 -auth 'admin:Adm!n321'`

to install a plugin from CLI
`java -jar jenkins-cli.jar -s http://localhost:8085 -auth 'admin:Adm!n321' install-plugin cloudbees-bitbucket-branch-source`

Then restart the jenkins 
`sudo service jenkins restart`

To uninstall a plugin from CLI 
`java -jar jenkins-cli.jar -s http://localhost:8085 -auth 'admin:Adm!n321' disable-plugin github -restart -r`

Best way to install a plugin is from UI. 

---

Role-based authorization strategy plugin

then later manage and assign roles based on the users

explored the Jenkins UI ✔

**System Administration with Jenkins**

resources: [https://www.jenkins.io/doc/book/system-administration/](https://www.jenkins.io/doc/book/system-administration/)

- backup
- restore
- monitor
- scale
- manage

Backups 

- full
- snapshots
- detrimental

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%201.png)

creating a backup - 3 ways

- filesystem snapshots
- plugins for backup - > better option “ Thinbackup plugin”
- write a shell script that backup the jenkin instance

restoring ✔

Monitoring with Prometheus ✔ 

**Pipelines**

**Jenkins file** —> is a text file that contains definitions, these definitions think templates, instructions. It tells the pipeline what it should be doing, and what services and plugins it should be interacting with.

How it looks like 

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%202.png)

Multi-stage pipeline

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%203.png)

Build a Jenkins File ✔

Build a multistage pipeline ✔

Build a CI pipeline ✔

Build a CD pipeline ✔

**What are build agents?**

The tasks like - deploying, building codes, run automated tests. also - A build agent is typically a machine, or container, which connects to a Jenkins controller and executes tasks when directed by the controller.

It is the system that runs the entire workload. Your entire CI/CD pipeline that's being run via these build agents, could be windows, macOS, Linux etc.

- task execution

**Why do we need to build agents?**

- you can actually run Jenkin’s workloads from the Jenkins server. Not recommended.  - for security purposes and performance concerns.
- Build server is the recommended way.

****Using an Ubuntu Server as a Build Agent****

Lets add a new user on the Jenkins server 

commands:

- sudo adduser newuser
- sudo usermod -aG sudo newuser - has admin permission
- groups newuser

then go to Jenkins dashboard —> manage jenkins —> manage credentials —> add credential 
After this go to manage node and clouds options 

then choose new node —> choose permanent agent

// no of executes means that the no. of concurrent builds that Jenkins may perform in this code.

**Using the new build agent for a CI/CD pipeline**

Step 1:

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%204.png)

Step 2:

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%205.png)

Step 3:

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%206.png)

Step 4:

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%207.png)

Now do build now and see the console. 

Ex: build a permanent agent 
step 1: 

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%208.png)

Step 2:

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%209.png)

Step 3:

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2010.png)

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2011.png)

Now let's create a job to run on Linux-run node.

Step 1:

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2012.png)

Step 2:

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2013.png)

step 3:

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2014.png)

Build now —> check console

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2015.png)

If asked to create a pipeline job then:

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2016.png)

**Using container build agents**

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2017.png)

**Blue Ocean**

- easier to use
- better sophisticated visualization
- fast and intuitive pipeline status
- pipeline editor
- personalization
- pinpoint precision
- native integration for branch and pull requests

**Installation of Blue Ocean** 

Manage plugins —> blue ocean —> <jenkins URL> /blue in subdomain and its done.

**Creating a pipeline in Blue Ocean**

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2018.png)

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2019.png)

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2020.png)

**Jenkins Security** 

User and access controls: Authentication and authorization.

**Common Jenkins security mistakes**

- anyone can do anything
- logged in users can do anything
- anonymous and authenticated users
- built in node - use rather build agents

**Ways to secure Jenkins**

- using security plugins
- exploring these options

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2021.png)

Using both the `Security Realm` and `Authorization` configurations it is possible to configure very relaxed or very rigid authentication and authorization schemes in Jenkins.

After enabling the role-based strategy plugin 

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2022.png)

By default, Jenkins runs its builds on the `built-in node` but is `inadvisable` in the long term.

It is due to the fact that any builds running on the built-in node have the same level of access to the controller file system as the Jenkins process.

Recommended Plugin

- matrix based security
- project-based matrix authorization

**LAB: DEPLOYING WITH MULTI-STAGE PIPELINE**

step 1: install plugins → ssh build agents and pipeline and restart Jenkins server

step 2: add credentials

step 3: add nodes

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2023.png)

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2024.png)

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2025.png)

Similarly do for prod.

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2026.png)

step 4: create and configure the job

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2027.png)

Choose pipeline

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2028.png)

And add the pipeline script.

Now building the pipeline – In progress

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2029.png)

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2030.png)

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2031.png)

Then check if app is deployed or not.

![Untitled](Jenkins%208ed686efb39048db8f26744669708ae0/Untitled%2032.png)

 

`withEnv ( ['JENKINS_NODE_COOKIE=do_not_kill'] )`, it is needed to make sure Jenkins doesn't kill the background process we are starting in this stage on the respective server.

---